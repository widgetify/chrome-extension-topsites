
chrome.storage.sync.get(['theTopSites'], function(items) {
    let message = document.getElementById("extension-message")
    message.innerText = ""
    let topSitesArray = items.theTopSites
    for (i=0;i<8;i++) {
        let listItem         = document.createElement("LI");
        let link             = document.createElement("a");
        let linkurl          = document.createElement("a");

        let displayTitle     = topSitesArray[i].title

        if (displayTitle.length>20) {
            displayTitle = topSitesArray[i].title.substr(0, 20) + "..."
        }

        let thirdSlashIndex  = topSitesArray[i].url.split("/", 3).join("/").length;
        let secondSlashIndex = topSitesArray[i].url.split("/", 2).join("/").length;
        let displayUrl       = topSitesArray[i].url.slice(secondSlashIndex+1, thirdSlashIndex)
        linkurl.className    = "link-url"
        linkurl.href         = topSitesArray[i].url;
        linkurl.title        = displayTitle
        linkurl.innerText    = displayUrl
           
        link.className       = "link-title"
        link.href            = topSitesArray[i].url;
        link.title           = displayTitle
        link.innerText       = displayTitle + " ";

        listItem.appendChild(link);
        listItem.appendChild(linkurl);

        document.getElementById("top-sites").appendChild(listItem)
    }
});