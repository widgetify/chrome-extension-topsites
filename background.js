// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';
    
var date_now = Date.now();
var dateObj = {date: date_now};

chrome.storage.sync.get(['date'], function(result) {
    console.log('Current date ' + result.date);
    var older_date = result.date
    if (!older_date || date_now - older_date > 1000*10){
        console.log("Enogh time passed, reseting topSites")
        chrome.topSites.get(function (topSites){
            console.log('topSites:', topSites)
            chrome.storage.sync.set({theTopSites: topSites}, function() {
                if(!chrome.runtime.lastError){
                    console.log('Value is set to ' + date_now);
                }
            });
        });

        chrome.storage.sync.set(dateObj, function() {
            if(!chrome.runtime.lastError){
                console.log('New date is ' + date_now);
            }   
        });
    }
});

